--CREATE DATABASE black_jack_esteban;

--CREATE SCHEMA app;

CREATE TABLE app.usuarios
(
	id SERIAL primary KEY,
	id_plataforma NUMERIC NOT NULL UNIQUE,
	nombre TEXT,
	url_foto TEXT,
	email TEXT NOT NULL,
	plataforma TEXT NOT NULL,
	saldo NUMERIC DEFAULT 0,
	partidas NUMERIC DEFAULT 0
);