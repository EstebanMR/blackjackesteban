﻿using System;
using System.Windows.Forms;

namespace BlackJackEsteban
{
    partial class frmFacebookLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navWeb = new System.Windows.Forms.WebBrowser();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // navWeb
            // 
            this.navWeb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navWeb.Location = new System.Drawing.Point(0, 0);
            this.navWeb.Margin = new System.Windows.Forms.Padding(4);
            this.navWeb.MinimumSize = new System.Drawing.Size(27, 25);
            this.navWeb.Name = "navWeb";
            this.navWeb.Size = new System.Drawing.Size(724, 403);
            this.navWeb.TabIndex = 0;
            this.navWeb.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            this.navWeb.Navigated += new System.Windows.Forms.WebBrowserNavigatedEventHandler(this.Navegar);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(584, 413);
            this.webBrowser1.TabIndex = 0;
            // 
            // frmFacebookLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 413);
            this.Controls.Add(this.webBrowser1);
            this.Name = "frmFacebookLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmFacebookLogin";
            this.Load += new System.EventHandler(this.frmFacebookLogin_Load);
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.WebBrowser navWeb;
        private WebBrowser webBrowser1;
    }
}