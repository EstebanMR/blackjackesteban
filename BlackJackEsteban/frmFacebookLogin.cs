﻿using BlackJackEsteban.BOL;
using BlackJackEsteban.ENTITIES;
using Facebook;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BlackJackEsteban
{
    public partial class frmFacebookLogin : Form
    {
        private EUsuario us;
        private FacebookBOL fBO;

        private string _accessToken;
        //public Usuario obtUsuario { get { return us; } }
        //UsuarioDao usDaO = new UsuarioDao();
        public frmFacebookLogin()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Carga todos los componentes necesarios para el frame 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmFacebookLogin_Load(object sender, EventArgs e)
        {
            fBO = new FacebookBOL();
            us = new EUsuario();
            string url = fBO.GenerateLoginUrl();
            Console.WriteLine(url);
            navWeb.Navigate(url);
            webBrowser1.Navigate(url);
        }

        /// <summary>
        /// carga el navegador 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Navegar(object sender, WebBrowserNavigatedEventArgs e)
        {
            if (navWeb.Visible)
            {
                FacebookClient fb = new FacebookClient();
                FacebookOAuthResult oauthResult;
                bool _authorized;
                if (fb.TryParseOAuthCallbackUrl(e.Url, out oauthResult))
                {
                    if (oauthResult.IsSuccess)
                    {
                        _accessToken = oauthResult.AccessToken;
                        _authorized = true;
                    }
                    else
                    {
                        _accessToken = "";
                        _authorized = false;
                    }
                    if (_authorized)
                    {
                        fb = new FacebookClient(_accessToken);
                        dynamic result = fb.Get("me");
                        us.Nombre= result.first_name+" "+result.last_name;
                        us.Email = result.email;
                        us.UrlFoto = string.Format("https://graph.facebook.com/{0}/picture",
                            result.id);
                        us.ID_Cuenta= Convert.ToInt32(result.id);
                        us.Plataforma= "Faceboook";
                        //UsuarioDal uDal = new UsuarioDal();
                        //uDal.Insertar(us);	

                        //UsuarioDao uDao = new UsuarioDao();

                        Console.WriteLine(us.Nombre);
                        Console.WriteLine(us.Email);
                        Console.WriteLine(us.ID_Cuenta);
                        Console.WriteLine(us.UrlFoto);
                        this.Visible = false;

                        //		usDaO.agregarUsuario(us);
                        //	MessageBox.Show(us.Nombre);
                        //	this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Error al iniciar sesión.");
                    }
                }
            }
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }
    }
}
