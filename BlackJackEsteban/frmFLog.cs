﻿using BlackJackEsteban.BOL;
using BlackJackEsteban.ENTITIES;
using Facebook;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlackJackEsteban
{
    public partial class frmFLog : Form
    {
        private FacebookBOL fBO;
        private EUsuario us;
        string url;

        private string _accessToken;

        public frmFLog()
        {
            InitializeComponent();
        }

        private void navWeb_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void frmFLog_Load(object sender, EventArgs e)
        {
            fBO = new FacebookBOL();
            us = new EUsuario();
            url = fBO.GenerateLoginUrl();
            //Console.WriteLine(url);
            navWeb.Navigate(url);
        }

        private void navWeb_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            if (navWeb.Visible)
            {
                FacebookClient fb = new FacebookClient();
                FacebookOAuthResult oauthResult;
                bool _authorized;
                if (fb.TryParseOAuthCallbackUrl(e.Url, out oauthResult))
                {
                    if (oauthResult.IsSuccess)
                    {
                        _accessToken = oauthResult.AccessToken;
                        _authorized = true;
                    }
                    else
                    {
                        _accessToken = "";
                        _authorized = false;
                    }
                    if (_authorized)
                    {
                        fb = new FacebookClient(_accessToken);

                        dynamic result = fb.Get("me");
                        dynamic pic = fb.Get("me/?fields=picture"); 
                        dynamic em = fb.Get("me/?fields=email");
                        us.Nombre = result.name + " " + result.last_name;
                        us.Email = em.email;
                        us.UrlFoto = /*string.Format("https://graph.facebook.com/{0}/picture",*/
                            pic.picture.data.url;
                        us.ID_Cuenta = result.id;
                        us.Plataforma = "Facebook";
                        navWeb.Navigate(new Uri(String.Format("https://www.facebook.com/logout.php?next={0}&access_token={1}", url, _accessToken)));


                        if (us.ID_Cuenta.Equals(""))
                        {
                            MessageBox.Show("Usuario inválido");
                            Close();

                        }
                        else
                        {
                            Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Error al iniciar sesión.");
                    }
                }
            }
        }

        public EUsuario GetUsuario()
        {
            return us;
        }
    }
}
