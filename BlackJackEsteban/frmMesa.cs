﻿using BlackJackEsteban.BOL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlackJackEsteban
{
    public partial class frmMesa : Form
    {
        UsuarioBOL usBol = new UsuarioBOL();
        public frmMesa()
        {
            InitializeComponent();
        }

        private void cerrarSesiónToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            usBol.user = null;
            Close();
        }
    }
}
