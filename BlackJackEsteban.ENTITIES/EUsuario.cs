﻿using System.Drawing;

namespace BlackJackEsteban.ENTITIES
{
    public class EUsuario
    {
        public int ID { get; set; }
        public string ID_Cuenta { get; set; }
        public string Nombre { get; set; }
        public string UrlFoto { get; set; }
        public string Email { get; set; }
        public string Plataforma { get; set; }
        public int Saldo { get; set; }
        public int Partidas { get; set; }
        //public Image Foto { get; set; }
    }
}
