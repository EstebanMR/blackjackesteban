﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackJackEsteban.ENTITIES;
using BlackJackEsteban.DAL;

namespace BlackJackEsteban.BOL
{
    public class UsuarioBOL
    {
        public EUsuario user = new EUsuario();
        public EUsuario Login(EUsuario us)
        {
            return new UsuarioDAL().Login(us);
        }
    }
}
